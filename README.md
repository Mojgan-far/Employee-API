# Getting Started with Create React App

This project is done by Mojgan F Farmahini as a code test for 13|37

### Getting Started

Download the project from github:
https://github.com/Mojgan360/Employee-API

===================================

### Client-side:

Install the node_modules:
$ npm install
$ npm start
Open http://localhost:3000/ to see the application.

===================================

### Server-side

Key Authorization is Invalid so you can see Application by fake server, but in "src/helpers/axios" could be change key-authorization.
I used json-server like a fake server
$ npm run server
Open http://localhost:5000/ to see the db.json on the server side.

===================================

### Test

$ npm run test

===================================

## deploy

Heaku

## Technologies

Following technologies have been used to develop this application.
● HTML
● CSS
● Javascript
● React - Hooks
● React Context: similar Redux --> (useReducer, useContext)
● Styled-component

Tools
● Visual Studio Code
● Postman for checking API

Test
● @testing-library

========================================

Design/accessibility

Support for color blindness (document what you’ve done): done! (use SimDalTonism) & Använd färgtonalitet

Responsive design, works on Filter by name and Integration tests of mobile and tablets: done

Screen reader functionality: not done

Use modern CSS throughout the application (css-grid, variables, clamp etc): done

Functionality

Sort by name & office: done

Filter by name and office:done

Enable switch between a grid and a different view (such as list) :done

Available on a free public url (such as Azure, Heroku): done

CI/CD pipeline from your repo (e.g. Travis, GitLab, Azure): done

Testing/QA

Use Typescript (or similar, no any’s!):done

Integration tests of mobile and tablets oce components:done

End-to-end testing (with an existing framework): not done

Unit tests for existing functionality (reasonable coverage: not done

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

# Employee-API
